import datetime

from brightsky.parsers import ObservationsParser
from dateutil.tz import tzutc


class ClimateHistoryParser(ObservationsParser):
    elements = {
        'monthly_temperature': 'MO_TT',
        'dwd_station_id': 'STATIONS_ID'
    }

    def parse_station_id(self, zf):
        return None

    def parse_lat_lon_history(self, zf, dwd_station_id):
        return {}

    def parse_reader(self, filename, reader, lat_lon_history):
        for row in reader:
            timestamp = datetime.datetime.strptime(
                row['MESS_DATUM_BEGINN'], '%Y%m%d').replace(tzinfo=tzutc())
            yield {
                'timestamp': timestamp,
                **self.parse_elements(row, None, None, None),
            }
