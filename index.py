import json
import sys
from ClimateHistoryParser import ClimateHistoryParser

import pandas as pd


def parse(url):
    """parse a url and get records"""
    parsers = ClimateHistoryParser(url=url)
    parsers.download()
    records = list(parsers.parse())
    parsers.cleanup()
    return records


# get args from cmd (exclude the first one which is the script name)
urls = sys.argv[1::]

# get records from urls
data = [parse(url) for url in urls]

# flattened records
flat_data = [item for sublist in data for item in sublist]

# remove duplicate records by timestamp
reduced = list(
    {dictionary['timestamp']: dictionary for dictionary in flat_data}.values())

# sort records by timestamp
sortedlist = sorted(reduced, key=lambda d: d['timestamp'])

# create pandas dataframe from records
df = pd.DataFrame(sortedlist)

# extract month and year from timestamp
df['month'] = pd.to_datetime(df['timestamp']).dt.month
df['year'] = pd.to_datetime(df['timestamp']).dt.year

# filter data and calculate monthly average from before 1900
before_1900_df = df[df['year'] < 1900]
mothly_mean_df = before_1900_df.groupby(
    ['month'], as_index=False).mean(numeric_only=True)


# calculate temperature deviation for each record
df['temperature_deviation'] = df.apply(lambda row: row['monthly_temperature'] -
                                       mothly_mean_df[mothly_mean_df['month'] ==
                                       row['month']]['monthly_temperature'].values[0], axis=1)

# format the timestamp
df['timestamp'] = df['timestamp'].dt.strftime('%Y-%m-%d')

# drop unused columns
df = df.drop(columns=['month', 'year'])

# create json array from data
json_df = df.to_json(orient='records', indent=4)

# return records as json array
print(json_df)
