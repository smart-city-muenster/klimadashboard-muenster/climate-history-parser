# Climate History Parser

This python script reads climate data from the [DWD (Deutscher Wetterdienst)](https://www.dwd.de/DE/Home/home_node.html) using the [Bright Sky](https://github.com/jdemaeyer/brightsky/) project. The script uses the monthly average temperatures and calculates the deviation from the monthly mean values from before 1900.

### Usage

You need to provide URL(s) to the climate data (see [https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/)). You can choose recent and historical data. The application need the `.zip` URLs.

```sh
pip install -r requirements.txt
python index.py [climate_history_urls] > climate_history.json
```

**Example**
This example fetches historical data from the former weatherstation Münster and the new weatherstation Münster/Osnabrück as well as recent data from Münster/Osnabrück. Duplicate records (by timestamp) are removed.

```sh
python index.py https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/historical/monatswerte_KL_03404_18530101_19911231_hist.zip https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/historical/monatswerte_KL_01766_19891001_20221231_hist.zip https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/recent/monatswerte_KL_01766_akt.zip > climate_history.json
```

### Output
The script creates a json array which can be written to a file. The file will look like this:
```json
[
   {
        "observation_type":"historical",
        "dwd_station_id":1766.0,
        "wmo_station_id":null,
        "timestamp":"2022-10-01",
        "monthly_temperature":12.76,
        "temperature_deviation":3.4646808511
    },
    {
        "observation_type":"historical",
        "dwd_station_id":1766.0,
        "wmo_station_id":null,
        "timestamp":"2022-11-01",
        "monthly_temperature":7.94,
        "temperature_deviation":3.470212766
    }
    ...
```

The `monthly_temperature` value is the actual reading while `temperature_deviation` is the difference from the monthly mean value from before 1900.

### Development
With the `.devcontainer` folder, you can immediately start using the script using a [VSCode Dev Container](https://code.visualstudio.com/docs/devcontainers/containers)

